<?php

namespace Database\Seeders;

use App\Models\Login;
use Illuminate\Database\Seeder;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Support\Facades\Hash;
use App\Models\User;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run(){
            User::firstOrCreate(
                    ['email' => 'admin@gmail.com'],
                    
                    [       
                        'name' => 'admin',
                        'password' =>  Hash::make('123'),
                        'rank'=>0
                    ]
            );
            Login::firstOrCreate(
                    ['login_email' => 'admin@gmail.com'],
                    
                    [       
                        'login_password' =>  Hash::make('123'),
                        'login_rank'=>0
                    ]
            );
        }
}
