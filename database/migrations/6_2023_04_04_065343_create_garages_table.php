<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('garages', function (Blueprint $table) {
            
            $table->id("garage_id");
            $table->string("garage_title");

            
            $table->unsignedBigInteger('garage_account_id');
            $table->unsignedBigInteger('garage_town_id');
            $table->foreign('garage_account_id')->references('account_id')->on('accounts')->cascadeOnDelete()->cascadeOnUpdate();
            $table->foreign('garage_town_id')->references('town_id')->on('towns')->cascadeOnDelete()->cascadeOnUpdate();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('garages');
    }
};
