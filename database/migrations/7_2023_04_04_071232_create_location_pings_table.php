<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('location_pings', function (Blueprint $table) {

            $table->id("location_ping_id");
            $table->boolean("location_ping_status");
            $table->date("location_ping_date_requested");
            $table->double("location_ping_latitude");       
            $table->double("location_ping_longitude");    

            $table->unsignedBigInteger("location_ping_account_id");
            $table->foreign("location_ping_account_id")->references("account_id")->on("accounts")->cascadeOnDelete()->cascadeOnUpdate();
        
        });
    }
    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('location_pings');
    }
};
