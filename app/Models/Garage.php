<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Garage extends Model
{
    use HasFactory;
    public $timestamps = False;


    /**
     * The primary key associated with the table.
     *
     * @var string
     */
    protected $primaryKey = "garage_id";
    
    protected $fillable = [
        "garage_title",
        'garage_account_id',
        'garage_town_id'

    ];


    public function town()
    {
        return $this->belongsTo(Town::class, 'garage_town_id');
    }

    public function account()
    {
        return $this->belongsTo(Account::class, 'garage_account_id');
    }
}
