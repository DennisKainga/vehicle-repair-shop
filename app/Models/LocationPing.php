<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class LocationPing extends Model
{
    use HasFactory;
    public $timestamps = False;

       /**
     * The primary key associated with the table.
     *
     * @var string
     */
    protected $primaryKey = "location_ping_id";

    protected $fillable = [
        
        "location_ping_status",
        "location_ping_date_requested",
        "location_ping_latitude",       
        "location_ping_longitude",     
        "location_ping_account_id"
    ];
}
