<?php

namespace App\Http\Controllers;

use App\Models\Account;
use App\Models\Garage;
use App\Models\Town;
use Illuminate\Http\Request;

class GarageController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index(){

        return view("garages.index",[
            "garages"=>Garage::all(),
            'towns'=>Town::select("town_id","town_name")->get(),
            'mechanics'=> Account::select("account_id","account_first_name",'account_last_name')->whereHas('login', function($query){$query->where('login_rank',0);})->get(),

            
        ]);
    }

   
    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request){
        
        $FormFields = $request->validate([
            "garage_name"=>["required","unique:garages,garage_title"],
            "town"=>["required"],
            "owner"=>["required"]
        ]);
        
        Garage::create([
            "garage_title"=>$request->garage_name,
            'garage_account_id'=>$request->owner,
            'garage_town_id'=>$request->town
        ]);

        return redirect()->back()->with("success_message","Garage Added successfully");

    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        //
    }

   

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, Garage $garage){
        $FormFields = $request->validate([
            "garage_name"=>["required"],
            "town"=>["required"],
            "owner"=>["required"]
        ]);

        $garage->update([
            "garage_title"=>$request->garage_name,
            'garage_account_id'=>$request->owner,
            'garage_town_id'=>$request->town

        ]);
        return redirect()->back()->with("success_message","Garage Updated successfully");
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(Garage $garage){
        $garage->delete();

        return redirect()->back()->with("info_message","Garage Deleted successfully");
        //
    }
}
