<?php

namespace App\Http\Controllers;

use App\Models\Town;
use App\Models\County;
use Illuminate\Http\Request;

class TownController extends Controller{

    protected $geocoordinatescontroller;

    public function __construct(GeoCoordinatesController $geocoordinatescontroller){
        $this->geocoordinatescontroller = $geocoordinatescontroller;
    }

    /**
     * Display a listing of the resource.
     */
    public function index(){
        return view("towns.index",[
            "towns"=>Town::all(),
            "counties"=> County::select("county_id","county_name")->get()
        ]);
    }


    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request){

        $FormFields = $request->validate([
            "town_name"=>["required","unique:towns,town_name"],
            "county_name"=>["required"]
        ]);

        // Get the latitude and longitude of the county
        $coordinates = $this->geocoordinatescontroller->getCoordinates($request->town_name);

        if (isset($coordinates['error'])) {
            // An error occurred while fetching the coordinates
            // dd($coordinates['error']);
            return redirect()->back()->with('error_message', $coordinates['error']);
        }      
        Town::create([

            "town_name"=>$request->town_name,
            "town_longitude"=>$coordinates["longitude"],
            "town_latitude"=>$coordinates["latitude"],
            "town_county_id"=>$request->county_name
        ]);

        return back()->with("success_message","Town added successfully");
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        //
    }


    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, Town $town){

        $FormFields = $request->validate([
            "town_name"=>["required"],
            "county_name"=>["required"]
        ]);
      
        $town->update([
            "town_name"=>$request->town_name,
            "town_county_id"=>$request->county_name
        ]);


        return back()->with("success_message","Town udpated successfully");
        //
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(Town $town){
        $town->delete();
        return redirect()->back()->with("info_message","Town Deleted successfully");
    }
}
