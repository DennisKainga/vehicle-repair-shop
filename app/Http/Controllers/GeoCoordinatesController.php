<?php

namespace App\Http\Controllers;


class GeoCoordinatesController extends Controller{
    public function getCoordinates($location_name){
        // Construct the Geocoding API request URL

        // GOOGLE_MAPS_API_KEY=AIzaSyCUZSVf1iMPlzrn5j-A5ZcNdqPUEN_DthE
        $request_url = 'https://maps.googleapis.com/maps/api/geocode/json?address=' . urlencode($location_name) . ',Kenya&key=' .config('services.maps.key');

        // Make the API request and get the response
        $response = file_get_contents($request_url);
        $data = json_decode($response, true);
        // dd($data);

        // Check if the API returned any errors
        if ($data['status'] != 'OK') {
            return ['error' => 'An error occurred while fetching coordinates.'];
        }

        // Extract the latitude and longitude from the response
        $lat = $data['results'][0]['geometry']['location']['lat'];
        $lng = $data['results'][0]['geometry']['location']['lng'];

        return ['latitude' => $lat, 'longitude' => $lng];
    }




}
