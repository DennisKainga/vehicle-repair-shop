<?php

namespace App\Http\Controllers;

use App\Models\County;
use Exception;
use Illuminate\Http\Request;

class CountyController extends Controller{

    protected $geocoordinatescontroller;

    public function __construct(GeoCoordinatesController $geocoordinatescontroller)
    {
        $this->geocoordinatescontroller = $geocoordinatescontroller;
    }


    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        //
        return view("counties.index",[
            "counties"=>County::all()
        ]);
    }

   
    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request){

        $FormFields = $request->validate([
            "county_name"=>['required','unique:counties,county_name']
        ]);

        // Get the latitude and longitude of the county
        $coordinates = $this->geocoordinatescontroller->getCoordinates($request->county_name);

        if (isset($coordinates['error'])) {
            // An error occurred while fetching the coordinates
            // dd($coordinates['error']);
            return redirect()->back()->with('error_message', $coordinates['error']);
        }      
        County::create([

            "county_name"=>$request->county_name,
            "county_longitude"=>$coordinates["longitude"],
            "county_latitude"=>$coordinates["latitude"]
        ]);
       
        return redirect()->back()->with("success_message","County added successfully");
    }

   
    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, County $county){

        $FormFields = $request->validate([
            "county_name"=>['required']
        ]);

        $county->update([
            "county_name"=>$request->county_name
        ]);

        return redirect()->back()->with("success_message","County updated successfully");
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(County $county){
        $county->delete();

        return redirect()->back()->with("info_message","County Deleted successfully");
        //
    }
}
