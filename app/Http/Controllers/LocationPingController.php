<?php

namespace App\Http\Controllers;

use App\Models\Account;
use App\Models\LocationPing;
use App\Models\Town;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class LocationPingController extends Controller{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        //
    }
    
    /**
     * Calculates the Haversine distance between two points given their GPS coordinates.
     */
    private function haversineDistance($lat1, $lon1, $lat2, $lon2) {
        $R = 6371; // Earth's radius in km
        $dLat = deg2rad($lat2 - $lat1);
        $dLon = deg2rad($lon2 - $lon1);
        $a = sin($dLat/2) * sin($dLat/2) + cos(deg2rad($lat1)) * cos(deg2rad($lat2)) * sin($dLon/2) * sin($dLon/2);
        $c = 2 * atan2(sqrt($a), sqrt(1-$a));
        $distance = $R * $c;
        return $distance;
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request){
    
        // Get the supplied GPS coordinates
        $latitude = $request->latitude;
        $longitude = $request->longitude;
    
        // Get all the towns from the database
        $towns = Town::all();
    
        // Initialize variables for storing the nearest town and its distance
        $nearest_town = null;
        $nearest_distance = INF;
    
        // Loop through each town and calculate its distance from the supplied coordinates
        foreach ($towns as $town) {
            $town_latitude = $town->latitude;
            $town_longitude = $town->longitude;
    
            $distance = $this->haversineDistance($latitude, $longitude, $town_latitude, $town_longitude);
    
            // Check if this town is closer than the current nearest town
            if ($distance < $nearest_distance) {
                $nearest_town = $town;
                $nearest_distance = $distance;
            }
        }
        $user_id = Auth::user()->id;
        $account = Account::where("account_login_id",$user_id)->first();
        // dd($account);
        // The $nearest_town variable now contains the closest town to the supplied coordinates
       LocationPing::create([
        "location_ping_status"=>0,
        "location_ping_date_requested"=>date('Y-m-d'),
        "location_ping_latitude"=>$latitude,
        "location_ping_longitude"=>$longitude,
        "location_ping_account_id"=> $account->account_id
       ]);
        return view("garages.index",[
            "garages"=>$nearest_town->garages()->get(),
            "mechanics"=>[],
            "towns"=>[]
        ]);

        // return redirect()->route("garage-index")->with("info_message","These are the garages based on your location");
        // dd();
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, string $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        //
    }
}
