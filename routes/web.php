<?php

use App\Http\Controllers\AccountController;
use App\Http\Controllers\AuthController;
use App\Http\Controllers\CountyController;
use App\Http\Controllers\GarageController;
use App\Http\Controllers\LocationPingController;
use App\Http\Controllers\TownController;
use Illuminate\Support\Facades\Route;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::prefix("/")->group(function(){

    Route::get("/", [AuthController::class, 'index'])->name('auth-index');

    Route::post("authenticate", [AuthController::class, 'authenticate'])->name('auth-authenticate');

    Route::get("logout", [AuthController::class, 'logout'])->name('auth-logout');

    Route::get('dashboard', [AuthController::class, "dashboard"])->name('dashboard');

});

Route::prefix("pings")->group(function(){

    Route::post("get-mechanics", [ LocationPingController::class, 'store'])->name('location-ping');

    
});

Route::prefix("accounts")->group(function(){

    Route::get("/{rank}", [AccountController::class, 'index'])->name("accounts-index");

    Route::post("account-store", [AccountController::class, 'store'])->name('account-store');

    Route::delete("account-delete/{account}", [AccountController::class, 'destroy'])->name('account-delete');

    Route::put("account-update/{account}",[AccountController::class, 'update'])->name('account-update');

});


Route::prefix("counties")->group(function(){

    Route::get("/", [CountyController::class, 'index'])->name("county-index");

    Route::post("county-store", [CountyController::class, 'store'])->name('county-store');

    Route::delete("county-delete/{county}", [CountyController::class, 'destroy'])->name('county-delete');

    Route::put("county-update/{county}", [CountyController::class, 'update'])->name("county-update");

});


Route::prefix("towns")->group(function(){

    Route::get("/", [TownController::class, 'index'])->name("town-index");

    Route::post("town-store", [TownController::class, 'store'])->name('town-store');

    Route::delete("town-delete/{town}", [TownController::class, 'destroy'])->name('town-delete');

    Route::put("town-update/{town}", [TownController::class, 'update'])->name("town-update");

});


Route::prefix("garages")->group(function(){
    
    Route::get("/", [GarageController::class, 'index'])->name("garage-index");

    Route::post("garage-store", [GarageController::class, 'store'])->name('garage-store');

    Route::delete('garage-delete/{garage}', [GarageController::class, 'destroy'])->name('garage-delete');
    
    Route::put('garage-update/{garage}', [GarageController::class, 'update'])->name('garage-update');

});