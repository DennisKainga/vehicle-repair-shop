<x-Layout title="Counties">
    
    <div class="row">
        
        @foreach ($counties as $county)
            
            {{-- Card to list all counties --}}
            <x-TextCard 
                title="{{ $county->county_name }}"
                longitude="{{$county->county_longitude}}"
                latitude="{{$county->county_latitude }}"
                id="{{$county->county_id}}"
                delete_route="{{ route('county-delete',$county->county_id) }}">
            </x-TextCard>
            
            {{-- edit form modal --}}
            <x-County.Form 
                title="{{'Edit'.' '.$county->county_name}}" 
                :county="$county"
                submit_route="{{ route('county-update',$county->county_id) }}"
                modal="{{'edit'.$county->county_id
            }}">
            </x-County.Form>


            {{-- Show Delete Modal --}}
            <x-DeleteModal 
            title="{{'Delete'.' '.$county->county_name}}"

            modal="{{'delete'.$county->county_id}}"
            
            delete_route="{{ route('county-delete',$county->county_id) }}">
            </x-DeleteModal>
        @endforeach
    
    </div>
    {{-- This creates the edit frorm --}}
    <x-County.Form title="Create"  submit_route="{{ route('county-store') }}" modal="add-new"></x-County.Form>



    <table class="table d-none" id="table">
        <thead>
          <tr>
            <th scope="col">#</th>
            <th scope="col">County Name</th>
            <th scope="col">Longitude</th>
            <th scope="col">Latitude</th>
          </tr>
        </thead>
        <tbody>
          @foreach ($counties as $i=>$county)
            
          <tr>
            <th scope="row">{{ $i+1 }}</th>
            <td>{{  $county->county_name}}</td>
            <td>{{ $county->county_longitude}}</td>
            <td>{{$county->county_latitude}}</td>
           
          </tr>
          @endforeach
         
        </tbody>
      </table>

</x-Layout>