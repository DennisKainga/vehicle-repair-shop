@props(["title","modal","towns","mechanics","garage"=>null,"submit_route"])

<x-Modal title="{{ $title }}" modal="{{ $modal }}">

    <form method="POST" action="{{ $submit_route }}">
        
        @csrf
        @if($garage == null)
        @method("POST")
        @else
        @method("PUT")
        @endif
        <div class="form-group">
            <label for="inputAddress">Garage Name</label>
            <input  value="{{ $garage ? $garage->garage_title : '' }}" type="text" name="garage_name" class="form-control" placeholder="Marcelo">
        </div>    
        
        <div class="form-row align-items-center">
            <div class="col-12 my-1">
            <label class="mr-sm-2" for="inlineFormCustomSelect">Town (Location)</label>
            <select class="custom-select mr-sm-2" name="town">
                @if($garage !=null)
                <option selected value="{{ $garage->garage_town_id }}">{{ $garage->town->town_name }}</option>
                @else
                    <option>Choose...</option>
                @endif
                @foreach ($towns as $town)
                    <option value="{{ $town->town_id }}">{{ $town->town_name }}</option>
                @endforeach
            </select>
            </div>
        </div>

        <div class="form-row align-items-center">
            <div class="col-12 my-1">
            <label class="mr-sm-2" for="inlineFormCustomSelect">Garage Owner</label>
            <select class="custom-select mr-sm-2" name="owner">
                @if($garage !=null)
                <option selected value="{{ $garage->garage_account_id }}">{{ $garage->account->account_first_name.' '.$garage->account->account_last_name }}</option>
                @else
                    <option>Choose...</option>
                @endif
                @foreach ($mechanics as $mechanic)
                    <option value="{{ $mechanic->account_id }}">{{ $mechanic->account_first_name.' '.$mechanic->account_last_name }}</option>
                @endforeach
                
            </select>
            </div>
        </div>

        <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            <button type="submit" class="btn btn-primary">Save changes</button>
        </div>
    </form>
</x-Modal>