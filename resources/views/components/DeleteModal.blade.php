@props(["title","modal","delete_route"])
<x-Modal title="{{$title}}" modal="{{$modal}}">
    <p class="text-danger">Are you sure you want to {{ $title }}</p>

    <form action="{{ $delete_route  }}" method="POST">
        @csrf
        @method("DELETE")
        <div class="d-flex justify-content-between">
            <button class="btn btn-success btn-sm" data-dismiss="modal" >Cancel</button>
            <button class="btn btn-danger btn-sm">Proceed</button>
        </div>
    </form>
</x-Modal>