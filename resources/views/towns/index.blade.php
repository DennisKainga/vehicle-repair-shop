<x-Layout title="Towns">
    <div class="row">
        @foreach ($towns as $town)

            {{-- show each town in a card --}}
            <x-TextCard
                title="{{ $town->town_name }}"

                longitude="{{$town->town_longitude}}"
                
                latitude="{{$town->town_latitude }}"
                
                id="{{$town->town_id}}"
                
                county="{{ $town->county->county_name }}">
            </x-TextCard>

            {{-- Show edit form --}}
            <x-Town.Form 
                title="{{'Edit'.' '.$town->town_name}}" 

                modal="{{'edit'.$town->town_id}}"

                :town="$town"

                submit_route="{{ route('town-update',$town->town_id) }}"

                :counties="$counties">
            </x-Town.Form>

            {{-- Show delete modal --}}
           <x-DeleteModal 
                title="{{'Delete'.' '.$town->town_name}}"

                modal="{{'delete'.$town->town_id}}"
                
                delete_route="{{ route('town-delete',$town->town_id) }}">
            </x-DeleteModal>

        @endforeach
    
    </div>
    {{-- show create form --}}
    <x-Town.Form 
        title="New Town" 
        modal="add-new"
        submit_route="{{ route('town-store') }}" 
        :counties="$counties">
    </x-Town.Form>


    <table class="table d-none" id="table">
        <thead>
          <tr>
            <th scope="col">#</th>
            <th scope="col">Town Name</th>
            <th scope="col">Longitude</th>
            <th scope="col">Latitude</th>
          </tr>
        </thead>
        <tbody>
          @foreach ($towns as $i=>$town)
            
          <tr>
            <th scope="row">{{ $i+1 }}</th>
            <td>{{  $town->town_name}}</td>
            <td>{{ $town->town_longitude}}</td>
            <td>{{ $town->town_latitude}}</td>
           
          </tr>
          @endforeach
         
        </tbody>
      </table>



     

</x-Layout>