<x-Layout title="Garages">

  <div class="row">
    @unless (sizeof($garages) == 0)
      
   
        @foreach ($garages as $garage)  
        {{-- show each town in a card --}}
          
          <x-TextCard
                
          title="{{ $garage->garage_title }}"

          longitude="{{ $garage->town->town_name }}"

          latitude="{{ $garage->town->county->county_name }}"
          
          id="{{ $garage->garage_id }}"

          contact="{{ $garage->account->account_telephone_number }}"
          
          county="{{ $garage->account->account_first_name.' '.$garage->account->account_last_name }}">
        </x-TextCard>

          {{-- show edit form --}}
          <x-Garage.Form 
            title="{{'Edit '.$garage->garage_title.' Garage'  }}" 
            modal="{{'edit'.$garage->garage_id}}"
            :garage="$garage"
            :mechanics="$mechanics"
            :towns="$towns"
            submit_route="{{ route('garage-update',$garage->garage_id) }}">
          </x-Garage.Form>


            {{-- Show Delete Modal --}}
            <x-DeleteModal 
            title="{{'Delete'.' '.$garage->garage_title.' Garage'}}"

            modal="{{'delete'.$garage->garage_id}}"
            
            delete_route="{{ route('garage-delete',$garage->garage_id) }}">
            </x-DeleteModal>

        @endforeach
      @endunless
  </div>

  {{-- show create form --}}
  <x-Garage.Form 
    title="New Garage" 
    modal="add-new"
    :mechanics="$mechanics"
    :towns="$towns"
    submit_route="{{ route('garage-store') }}">
  </x-Garage.Form>



  <table class="table d-none" id="table">
    <thead>
      <tr>
        <th scope="col">#</th>
        <th scope="col">Garage Name</th>
        <th scope="col">Garage owner</th>
        <th scope="col">County Location</th>
        <th scope="col">Town Location</th>
        <th scope="col">Longitude</th>
            <th scope="col">Latitude</th>
       
      </tr>
    </thead>
    <tbody>
      @foreach ($garages as $i=>$garage)
        
      <tr>
        <th scope="row">{{ $i+1 }}</th>
        <td>{{  $garage->garage_title}}</td>
        <td>{{ $garage->town->county->county_name}}</td>
        <td>{{ $garage->account->account_first_name.' '.$garage->account->account_last_name}}</td>
        <td>{{ $garage->town->town_name}}</td>
        <td>{{ $garage->town->town_longitude}}</td>
        <td>{{ $garage->town->town_latitude}}</td>
       
       
      </tr>
      @endforeach
     
    </tbody>
  </table>

</x-Layout>